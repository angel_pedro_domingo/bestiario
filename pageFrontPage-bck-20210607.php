<?php
/**
 * Template Name: Page FrontPage Slider
 */

get_header('slider');

$args = array(
    'numberposts' => 3,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'movie',
    'post_status' => 'publish',
    'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts($args);
$recent_posts1 = wp_get_recent_posts($args);


$argsResenia = array(
    'numberposts' => 4,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'resenia',
    'post_status' => 'publish',
    'suppress_filters' => true
);
$resenias = wp_get_recent_posts($argsResenia);

//foreach ($resenias as $p ){
//    $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($p['ID']), 'single-post-thumbnail');
//    var_dump($imageUrl[0]);
//    echo "<br>";
//
//}

//echo get_stylesheet_directory_uri();
?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12" style="padding-left: 1px; padding-right: 1px;">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php
                        $i = 0;
                        foreach ($recent_posts as $p) {
                            //$imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($p['ID']));
			    $imageUrl = get_the_post_thumbnail_url($p['ID'], 'custom-cine-slider-main');
				//var_dump($imageUrl);
                            if ($i == 0)
                                echo "<div class='carousel-item active'>";
                            else
                                echo "<div class='carousel-item'>";
                            echo "<img class='d-block w-100' src='" . $imageUrl . "' alt='".$p['post_title']."' >";

                            echo "<div class=\"carousel-caption text-success d-none d-sm-block\">";
                            echo "<h5 class='carousel-cine-title'>".$p['post_title']."</h5>";
                            echo "<button onclick='location.href=\"".get_permalink( $p['ID'])."\";' class='btn btn-outline-primary btn-lg'>Más Información</button>";

                            echo "</div>";
                            echo "</div>";
                            echo "";
                            echo "";
                            $i++;
//                var_dump($imageUrl[0]);
//                echo "<br>";

                        }
                        ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
                <?php
                $i = 0;
                foreach ($recent_posts1 as $p ){
                    $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($p['ID']), 'custom-cine-slider-main-150');
                    $urlYoutube = get_field('url_youtube', $p['ID']);
                    switch ($i) {
                        case 0:
                            echo "<div class='col-md-4' style='padding-left: 0; padding-right: 30px; text-align: right;'>";
                            break;
                        case 2:
                            echo "<div class='col-md-4' style='padding-right: 0; padding-left: 30px; text-align: right;'>";
                            break;
                        default:
                            echo "<div class='col-md-4' style='text-align: right;'>";
                            break;
                    }

                    if(strcmp($urlYoutube, '') == 0){
                        echo "<img class='d-block w-100' src='".$imageUrl[0]."' alt='First slide'>";
                        echo "<a class='btn btn-link btn-link-cine' href='".get_permalink( $p['ID'])."'>".$p['post_title']."</a>";
                    } else {
                        echo '<div class="embed-responsive embed-responsive-16by9">';
                        echo '<iframe class="embed-responsive-item" src="'.$urlYoutube.'?rel=0" allowfullscreen></iframe>';
                        echo '</div>';
                        echo '<div>';
                        echo '<a href="'.get_permalink( $p['ID']).'" class="btn btn-link btn-link-cine">Ver más</a>';
                        echo '</div>';
                    }
                    echo "</div>";
                    echo "";

                    $i++;
                }
                ?>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12" style="padding-left: 0">
                <h4 class="carousel-cine-title">RESEÑAS</h4>
            </div>
        </div>
        <div class="row">
            <?php
            $i=0;
            foreach ($resenias as $p ){
                $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($p['ID']), 'single-post-thumbnail');
                if($i % 2 == 0){
                    echo "<div class='col-md-6' style='padding-left: 0;'>";
                    echo "<div class='row'>";
                    echo "<div class='col-md-3'>";
                } else {
                    echo "<div class='col-md-6' style='padding-right: 0;'>";
                    echo "<div class='row'>";
                    echo "<div class='col-md-3'>";
                }

                if(isset($imageUrl[0])) {
                    echo "<img class='d-block w-100' src='".$imageUrl[0]."' alt='First slide'>";
                } else {
                    echo "<img class='d-block w-100' src='".get_stylesheet_directory_uri().'/images/no-photo.png'."' />";
                }
                echo "</div>";
                echo "<div class='col-md-9' style='text-align: justify; text-justify: inter-word;'>";
                echo "<h5 style='color: #3f3b87;'>".get_post_field('post_title', $p['ID'])."</h5>";
                echo "<p>". wp_trim_words( get_post_field('post_content', $p['ID']), 40, ' <a href="'.get_permalink($p['ID']).'" class="btn btn-link btn-link-cine">leer más</a>' )."</p>";
                echo "</div>";
                echo "</div>";
                echo "</div>";

                $i++;

            }
            ?>
        </div>

        <section id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <?php
                //			while ( have_posts() ) : the_post();
                //				get_template_part( 'template-parts/content-movie-cortas', 'page' );
                //                // If comments are open or we have at least one comment, load up the comment template.
                //                if ( comments_open() || get_comments_number() ) :
                //                    comments_template();
                //                endif;
                //
                //			endwhile; // End of the loop.
                ?>

            </main><!-- #main -->
        </section><!-- #primary -->
    </div>

<?php
get_sidebar();
get_footer();
