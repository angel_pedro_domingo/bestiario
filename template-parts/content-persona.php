<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

$args = array(
    'post_parent' => 0,
    'post_type' => 'movie',
    'numberposts' => -1,
);

$movies = get_children($args);

$str_directores = htmlPeliculasByPersona($movies, 'directores');
$str_guion = htmlPeliculasByPersona($movies, 'guionistas');
$str_actor = htmlPeliculasByPersona($movies, 'elenco');
$str_productor_ejecutivo = htmlPeliculasByPersona($movies, 'produccion_ejecutiva');
$str_asistente_de_direccion = htmlPeliculasByPersona($movies, 'asistente_de_direccion');
$str_direccion_fotografica = htmlPeliculasByPersona($movies, 'direccion_fotografica');
$str_montaje_edicion = htmlPeliculasByPersona($movies, 'montaje_edicion');
$str_musica = htmlPeliculasByPersona($movies, 'musica');
$str_direccion_de_arte = htmlPeliculasByPersona($movies, 'direccion_de_arte');
$str_sonido = htmlPeliculasByPersona($movies, 'sonido');
$str_maquillaje = htmlPeliculasByPersona($movies, 'maquillaje');
$str_vestuario = htmlPeliculasByPersona($movies, 'vestuario');
$str_decorado = htmlPeliculasByPersona($movies, 'decorado');

$criticas = $wpdb->get_results( "select p.id, p.post_title from $wpdb->postmeta pm inner join $wpdb->posts p on p.ID=pm.post_id where pm.meta_key like 'persona_%_critico_p' AND pm.meta_value=".get_the_ID(), OBJECT );
$str_criticas = htmlCriticasByPersona($criticas);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--    <div class="post-thumbnail">-->
<!--        --><?php //the_post_thumbnail(); ?>
<!--    </div>-->
    <header class="entry-header entry-header-cine">
        <?php
        the_title(
            '<h1 class="entry-title">',
            "<span>  (" . get_field('fecha_de_nacimiento') . ")</span></h1>");
        ?>
    </header><!-- .entry-header -->
    <div class="row">
        <div class="col-md-8">
            <?= get_the_content(); ?>
        </div>
        <div class="col-md-4">
            <?php the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Actor']); ?>
        </div>
    </div>
    <div class="entry-content cb_sinopsis">
        <hr class="hr_cineboliviano">
        <?php if (strcmp($str_criticas, '') != 0) { ?>
        <h4>Criticas:</h4>
        <?= $str_criticas ?>
        <hr class="hr_cineboliviano">
        <?php }
        if (strcmp($str_directores, '') != 0) { ?>
            <h4>Director:</h4>
            <?= $str_directores ?>
            <hr class="hr_cineboliviano">
        <?php }
        if (strcmp($str_guion, '') != 0) { ?>
            <h4>Guionista:</h4>
            <div class="row">
                <div class="col-md-12">
                    <?= $str_guion ?>
                </div>
            </div>
            <hr class="hr_cineboliviano">
        <?php }
        if (strcmp($str_actor, '') != 0) { ?>
            <h4>Actor:</h4>
            <div class="row">
                <div class="col-md-12">
                    <?= $str_actor ?>
                </div>
            </div>
            <hr class="hr_cineboliviano">
        <?php }
        if (strcmp($str_productor_ejecutivo, '') != 0) { ?>
            <h4>Productor ejecutivo:</h4>
            <div class="row">
                <div class="col-md-12">
                    <?= $str_productor_ejecutivo ?>
                </div>
            </div>
            <hr class="hr_cineboliviano">
        <?php }
        if (strcmp($str_asistente_de_direccion, '') != 0) { ?>
            <h4>Asistente de direción:</h4>
            <div class="row">
                <div class="col-md-12">
                    <?= $str_asistente_de_direccion ?>
                </div>
            </div>
            <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_direccion_fotografica, '') != 0){?>
        <h4>Director fotográfico:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_direccion_fotografica ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_montaje_edicion, '') != 0){?>
        <h4>Encargado de montaje y edición:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_montaje_edicion ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_musica, '') != 0){?>
        <h4>Encargado musica:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_musica ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_direccion_de_arte, '') != 0){?>
        <h4>Director de arte:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_direccion_de_arte ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_sonido, '') != 0){?>
        <h4>Director de sonido:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_sonido ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_maquillaje, '') != 0){?>
        <h4>Maquillista:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_maquillaje ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_vestuario, '') != 0){?>
        <h4>Encargado de vestuario:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_vestuario ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } if(strcmp($str_decorado, '') != 0){?>
        <h4>Decorador:</h4>
        <div class="row">
            <div class="col-md-12">
                <?= $str_decorado ?>
            </div>
        </div>
        <hr class="hr_cineboliviano">
        <?php } //if(strcmp($str_actor, '') != 0){?>

    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php wp_bootstrap_starter_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->