<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:59 PM
 */

/**
* Hola
**/
?>
<div class="col-md-4">
    <?= "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'></a>" ?>
    <?php the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Actor']); ?>
    <h3 style="text-align: center;"><a href="<?=get_permalink()?>"><?= the_title() ?></a></h3>
</div>
