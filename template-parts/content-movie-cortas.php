<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:31 PM
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'movie',
    'meta_query' => array(
        array(
            // Get the "price" custom field.
            'key' => 'tipo',
            // Set the price values.
            'value' => 'corto',
            // Set the compare operator.
            'compare' => '=',
            // Only look at numeric fields.
            'type' => 'string'
        )
    ),
    'posts_per_page' => 10,
    'paged' => $paged,
    'orderby' => 'title',
    'order' => 'ASC'
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {

    while ( $my_query->have_posts() ) {
        $my_query->the_post();
        get_template_part( 'template-parts/movie', 'page' );
    }

    if (function_exists("pagination")) {
        pagination($my_query->max_num_pages);
    }

} else {
    echo "Sin datos en la consulta!!";
}