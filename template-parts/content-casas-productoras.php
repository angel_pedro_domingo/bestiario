<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:31 PM
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'casa_productora',
//    'meta_query' => array(
//        array(
//            // Get the "price" custom field.
//            'key' => 'tipo',
//            // Set the price values.
//            'value' => 'largometraje',
//            // Set the compare operator.
//            'compare' => '=',
//            // Only look at numeric fields.
//            'type' => 'string',
//        )
//    ),
    'posts_per_page' => 6,
    'paged' => $paged
);

$my_query = new WP_Query( $args );
if ( $my_query->have_posts() ) {
    echo "<div class='row'>";
    while ( $my_query->have_posts() ) {
        $my_query->the_post();
        get_template_part( 'template-parts/casaproductora', 'page' );
    }
    echo "</div>";
    if (function_exists("pagination")) {
        pagination($my_query->max_num_pages);
    }

} else {
    echo "Sin datos en la consulta!!";
}