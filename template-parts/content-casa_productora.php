<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */


set_query_var( 'casa_productora_id', get_the_ID() );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header entry-header-cine">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'casa_productora' === get_post_type() ) : ?>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <?= get_the_content(); ?>
                        <hr class="hr_cineboliviano_azul">
                        <div class="row">
                            <div class="col-md-4">Fecha fundación</div>
                            <div class="col-md-8"><?=get_field('fecha_fundacion')?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Persona de contacto</div>
                            <div class="col-md-8"><?=get_field('persona_de_contacto')?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Dirección</div>
                            <div class="col-md-8"><?=get_field('direccion_fisica')?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Teléfono contacto</div>
                            <div class="col-md-8"><?=get_field('telefono_contacto')?></div>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-4">
                        <?php the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Casa productora']); ?>
                    </div>
                </div>
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
        get_template_part('template-parts/casaproductora_peliculas');
		?>
	</div><!-- .entry-content -->

    <?php cineboliviano_posted_on(); ?>
	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
