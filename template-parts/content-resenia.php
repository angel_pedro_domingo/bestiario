<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

$personas = get_field('persona');

$linksCriticos = htmlCriticosListLink($personas);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--    <div class="post-thumbnail">-->
<!--        --><?php //the_post_thumbnail(); ?>
<!--    </div>-->
    <header class="entry-header entry-header-cine">
        <?php
        the_title(
            '<h1 class="entry-title">',
            "<span></span></h1>");
        ?>
    </header><!-- .entry-header -->
    <div class="row">
        <div class="col-md-8">
            <?= get_the_content(); ?>
        </div>
        <div class="col-md-4">
            <?php the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Actor']); ?>
        </div>
    </div>
    <div class="entry-content cb_sinopsis">
        <hr class="hr_cineboliviano">
        <?php if (strcmp($linksCriticos, '') != 0) { ?>
        <h4>Criticos:</h4>
        <?= $linksCriticos ;?>
        <hr class="hr_cineboliviano">
        <?php }?>
    </div>


    <footer class="entry-footer">
        <?php wp_bootstrap_starter_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->