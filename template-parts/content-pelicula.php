<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

//$args = array(
//    'taxonomy' => 'idioma'
//);
//wp_list_categories($args);


$term_list = wp_get_post_terms(get_the_ID(), 'idioma', array("fields" => "all"));
$idiomas = "";
foreach ($term_list as $termi){
    $idiomas .= "/".$termi->name;
}

$args = array(
    'post_parent' => 0,
    'post_type'   => 'equipo_tecnico',
    'numberposts' => -1,
);
$limit_characters = 200;
$children = get_children( $args );
$guion = "";
$director = "";
$productor = "";
$actores = null;
foreach ($children as $child){
    $guion = substr(strip_tags(get_post_field('guion', $child->ID)), 0, $limit_characters).' ...';
    $director = get_post_field('director', $child->ID);
    $productor = get_post_field('produccion', $child->ID);
    $actores = get_post_field('elenco', $child->ID);
}

$str_actores = "";
foreach ($actores as $actor){
    echo $actor."|";
    $str_actores .= get_post_field('nombre', $actor)." - ";
}
$str_actores = substr($str_actores ,0, -3);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header entry-header-cine">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', "<span> /".get_field('anio'). "/".get_field('duracion')."/".get_field('duracion').$idiomas.'</span></h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'pelicula' === get_post_type() ) : ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <img src="<?php the_field('afiche'); ?>" class="img-fluid">
                    </div>
                    <div class="col-lg-4 col-md-4">
                        Fotografias
                    </div>
                    <div class="col-lg-4 col-md-4 ficha_tecnica">
                        <h4>Ficha Técnica</h4>
                        <h6>Sinopsis</h6>
                        <p><?php echo substr(strip_tags(get_field('sinopsis')), 0, $limit_characters)." ..."; ?></p>
<!--                        <p>--><?php //echo get_field('sinopsis'); ?><!--</p>-->
                        <p><span>Director: </span><?= $director ?></p>
                        <p><span>Guion: </span><?php echo $guion; ?></p>
                        <p><span>Productor: </span><?= $productor ?></p>
                        <p><span>Protagonistas: </span><?=$str_actores?></p>
                    </div>
                </div>
			<?php cineboliviano_posted_on(); ?>
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
//        if ( is_single() ) :
//			the_content();
//
//        else :
//            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
//        endif;
//
//			wp_link_pages( array(
//				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
//				'after'  => '</div>',
//			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
