<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:59 PM
 */

?>
<div class="col-md-6 row">
    <div class="col-md-6">
        <div style="height: 5px;"></div>
        <?php if(get_the_post_thumbnail_url(null, 'full')) {?>

            <img src="<?=get_the_post_thumbnail_url(null, 'full')?>" alt="Imagen" class="img-fluid">
        <?php } else {?>
            <img src="<?=get_stylesheet_directory_uri().'/images/user-silhouette.png'?>" alt="Imagen" class="img-fluid">
        <?php }?>
    </div>
    <div class="col-md-6">
        <a href="<?=get_permalink()?>"><?=the_title()?></a>
        <p><?=wp_trim_words(get_the_content(), 40)?></p>
    </div>

<!--    --><?//= "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='" . get_field('afiche') . "' /></a>" ?>
<!--    --><?php //the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Actor']); ?>
<!--    <h3 style="text-align: center;"><a href="--><?//=get_permalink()?><!--">--><?//= the_title() ?><!--</a></h3>-->
</div>
