<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:31 PM
 */

global $wpdb;

// Pagination Setup
$posts_per_page = 10;
$start = 0;
$paged = get_query_var( 'paged') ? get_query_var( 'paged', 1 ) : 1; // Current page number
$start = ($paged-1)*$posts_per_page;

$search_filter = '';
if(isset( $_GET['search_critico'])){
    $search_filter = $_GET['search_critico'];
}

$resultsCount = $wpdb->get_results( "select SQL_CALC_FOUND_ROWS distinct pm.meta_value from $wpdb->postmeta pm inner join $wpdb->posts p on p.ID=pm.meta_value where pm.meta_key like 'persona_%_critico_p' AND p.post_title like '%$search_filter%'", OBJECT );

$total_posts = $wpdb->get_var(
    "SELECT FOUND_ROWS();"
);

$results = $wpdb->get_results( "select distinct pm.meta_value, p.post_title, p.post_content from $wpdb->postmeta pm inner join $wpdb->posts p on p.ID=pm.meta_value where pm.meta_key like 'persona_%_critico_p' AND p.post_title like '%$search_filter%' order by p.post_title LIMIT $start, $posts_per_page", OBJECT );

echo "<div class='col-md-12'><div class='row'><div class='col-md-12'>";
echo "<form method='get'>";
//echo "<input type='text' name='search_critico' value='".$search_filter ."'>";
//echo "<input type='submit' value='Buscar'>";

echo "<div class='input-group mb-3'>";
echo "<input type='text' class='form-control' name='search_critico' value='".$search_filter ."' placeholder='Buscar criticos' aria-label='Buscar' aria-describedby='button-addon2'>";
echo "<div class='input-group-append'>";
echo "<button class='btn btn-outline-secondary' type='submit' id='button-addon2'>Buscar</button>";
echo "</div>";
echo "</div>";

echo "</form><br>";
echo "</div></div>";
foreach ( $results as $result )
{
    $image_url = get_the_post_thumbnail_url($result->meta_value, 'thumbnail');
    echo "<div class='row'>";
    echo "<div class='col-md-2'>";
    if(strcmp($image_url, '') != 0){
        echo "<img src='".get_the_post_thumbnail_url($result->meta_value, 'medium')."' class='img-thumbnail rounded' />";
    } else {
        echo "<img class='img-thumbnail rounded' src='".get_stylesheet_directory_uri().'/images/no-photo.png'."' />";
    }
    echo "</div>";
    echo "<div class='col-md-10'>";
    echo "<a href='".get_permalink($result->meta_value)."'>".$result->post_title."</a><br />" ;
    echo wp_trim_words( $result->post_content, 50);
    echo "</div>";
    echo "</div>";
    echo "<div style='height: 5px;'></div>";
    //get_template_part( 'content', 'category' );
}

// Display Pagination
$total_page = ceil( $total_posts / $posts_per_page); // Calculate Total pages

echo "<div class='row'><div class='col-md-12' style='text-align: center;'>";
pagination($total_page);
echo "</div></div></div>";
//echo $total_posts;
?>
