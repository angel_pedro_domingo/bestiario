<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

$fotogramas = get_field('fotogramas');

$idiomas = get_the_terms(get_the_ID(), 'idioma');
//var_dump($idiomas);
$str_idiomas = "";
foreach ($idiomas as $idioma) {
//    echo "xxxxxxxxxxxxx::".$idioma->name;
    $str_idiomas .= $idioma->name . ", ";
}
if(strcmp($str_idiomas, '') != 0)
    $str_idiomas = substr($str_idiomas, 0, -2);

//var_dump(the_post());
$resumen = get_field('duracion')." min";
if(strcmp($str_idiomas, '') != 0){
    $resumen .= ' | '.$str_idiomas;
}
?>
<style>
    .youtube-player {position: relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin: 0;}
    .youtube-player iframe {position:absolute;top: 0;left:0;width:100%;height:100%;z-index:100;background: transparent;}
    .youtube-player img {bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top: 0;border:none;height:auto;cursor:pointer;-webkit-transition: .4s all;-moz-transition: .4s all;transition: .4s all;}
    .youtube-player img:hover {-webkit-filter:brightness(75%);}
    .youtube-player .play {height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background: url("//i.imgur.com/TxzC70f.png") no-repeat;}
</style>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header entry-header-cine">
        <?php
        if (is_single()) :
            the_title(
                '<h1 class="entry-title">',
                "<span> (" . get_field('anio')." | ". $resumen . ")". '</span></h1>');
        else :
            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        endif;
        if ('movie' === get_post_type()) : ?>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <img src="<?php the_field('afiche'); ?>" class="img-fluid">
                </div>
                <div class="col-md-8">
                    <?php
                    if(get_field('url_youtube')):
                    ?>
                        <div class="youtube-player" data-id="<?=get_youtube_id_from_url_bestiario(get_field('url_youtube'))?>"></div>
                    <?php
                    else:
                    ?>
                        SIN url de youtube
                    <?php
                    endif
                    ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-<?= (is_array(get_field('resenas')))? '8' : '12' ?> ficha_tecnica">
                    <h4>SINOPSIS</h4>
                    <?= get_field('sinopsis'); ?>

                    <div style="height: 2px;"></div>
                    <?php if (is_array(get_field('directores'))) { ?>
                        <h4>EQUIPO ARTÍSTICO  Y TÉCNICO</h4>
                        <div class="row">
                            <div class="col-md-4"><h6>Director:</h6></div>
                            <div class="col-md-8"><p><?= htmlPersonas(get_field('directores')); ?></p></div>
                        </div>
                        <?php
                    }
                    if (is_array(get_field('productores'))) {
                        ?>
                        <hr class="hr_cineboliviano">
                        <div class="row">
                            <div class="col-md-4"><h6>Productores:</h6></div>
                            <div class="col-md-8"><p><?= htmlPersonas(get_field('productores')); ?></p></div>
                        </div>
                        <?php
                    }
                    if (is_array(get_field('guionistas'))) {
                        ?>
                        <hr class="hr_cineboliviano">
                        <div class="row">
                            <div class="col-md-4"><h6>Guión:</h6></div>
                            <div class="col-md-8"><p><?= htmlPersonas(get_field('guionistas')); ?></p></div>
                        </div>
                        <?php
                    } ?>
                    <p>
                        <a class="btn btn-link" style="padding-left: 0; text-decoration: none; color:#125EAF; font-size: 1.2rem; " data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Ver equipo completo <i class="fas fa-caret-down"></i>
                        </a>
                    </p>
                    <div class="collapse" id="collapseExample">
                        <?php if (is_array(get_field('elenco'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Elenco:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('elenco')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('produccion_ejecutiva'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Producción ejecutiva:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('produccion_ejecutiva')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('asistente_de_direccion'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Asistente de dirección:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('asistente_de_direccion')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('direccion_fotografica'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Fotografia:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('direccion_fotografica')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('montaje_edicion'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Montaje - Edición:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('montaje_edicion')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('musica'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Música:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('musica')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('direccion_de_arte'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Dirección de arte:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('direccion_de_arte')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('sonido'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Sonido:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('sonido')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('maquillaje'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Maquillaje:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('maquillaje')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('vestuario'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Vestuario:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('vestuario')); ?></p></div>
                            </div>
                            <?php
                        }
                        if (is_array(get_field('decorado'))) {
                            ?>
                            <hr class="hr_cineboliviano">
                            <div class="row">
                                <div class="col-md-4"><h6>Decorado:</h6></div>
                                <div class="col-md-8"><p><?= htmlPersonas(get_field('decorado')); ?></p></div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <h4>GALERÍA</h4>
                    <div class="row no-gutters" style="background: #000000;">
                        <?php
                        $i = 0;
                        foreach ($fotogramas as $fotograma) {

                            if (strcmp($fotograma, '') != 0) {
                                echo "<div class='col-md-2' style='display: flex; align-items: center;'>".fancyBoxImagen($i, $fotograma, $fotograma)."</div>";
//                        echo fancyBoxImagen($i, $fotograma, $fotograma);
                                $i++;
                            }

                        }
                        ?>
                    </div>
                </div>

                <?php if (is_array(get_field('resenas'))): ?>
                <div class="col-md-4" style="background-color: #dcdcdc; border: dashed 2px">
                        <div class="row">
                            <div class="col-md-12 ficha_tecnica">
                                <h4>RESEÑAS:</h4>
                                <hr class="hr_cineboliviano_azul">
                            </div>
                        </div>
                        <div class="row">
                            <?= htmlResenia(get_field('resenas')); ?>
                        </div>
                </div>

               <?php endif; ?>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 ficha_tecnica">
                    <hr class="hr_cineboliviano_azul">
                    <h4>CASAS PRODUCTORAS</h4>
                </div>
            </div>
            <div class="row">
                <?= htmlCasaProductora(get_field('casa_productora')); ?>
            </div>

            <?php cineboliviano_posted_on(); ?>
        <?php
        endif; ?>
    </header><!-- .entry-header -->
    <div class="entry-content">
        <?php
        //        if ( is_single() ) :
        //			the_content();
        //
        //        else :
        //            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
        //        endif;
        //
        //			wp_link_pages( array(
        //				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
        //				'after'  => '</div>',
        //			) );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php wp_bootstrap_starter_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->

<script type="text/javascript">
    jQuery(function () {
        jQuery('.link-cine-per-popover').popover({
            "trigger": "hover",
            "html": true,
            "content": function(){
                return templatePopup(this);
            }
        })
    });

    function templatePopup(link) {
        html = "<div class=\"row\">";
        html += "<div class=\"col-md-6\">";
        html += "<h6 class='cine-popup-nombre'>"+jQuery(link).data('nombre')+"</h6>";
        html += "<p class='cine-popup-resumen'>"+jQuery(link).data('info')+"</p>";
        html += "</div>";
        html += "<div class='col-md-6'>";
        html += "<img src='"+jQuery(link).data('img')+"' class='img-fluid' />";
        html += "</div>";
        html += "</div>";
        html += "";

        return html;

    }


    document.addEventListener("DOMContentLoaded",
        function() {
            var div, n,
                v = document.getElementsByClassName("youtube-player");
            for (n = 0; n < v.length; n++) {
                div = document.createElement("div");
                div.setAttribute("data-id", v[n].dataset.id);
                div.innerHTML = labnolThumb(v[n].dataset.id);
                div.onclick = labnolIframe;
                v[n].appendChild(div);
            }
        });
    function labnolThumb(id) {
        var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
            play = '<div class="play"></div>';
        return thumb.replace("ID", id) + play;
    }
    function labnolIframe() {
        var iframe = document.createElement("iframe");
        var embed = "https://www.youtube.com/embed/ID?autoplay=1";
        iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        this.parentNode.replaceChild(iframe, this);
    }
</script>
