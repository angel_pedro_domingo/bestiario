<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:59 PM
 */

?>
<div class="row">
    <div class="col-md-2">
        <?= "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='" . get_field('afiche') . "' /></a>" ?>
    </div>
    <div class="col-md-10 cb_archive_movie" style="padding-top: 5px;">
        <h4 class="entry-title"><a href="<?=get_permalink()?>"><?php the_title(); ?></a></h4>
        <hr class="hr_cineboliviano">
        <p>
            <b>Año:</b> <?= get_field('anio') ?> -
            <b>Duracion:</b> <?= get_field('duracion') ?> min.
        </p>
        <p style="text-align: justify; color: #080530;"><?= wp_trim_words(get_field('sinopsis'), 150) ?></p>
    </div>
</div>
<hr class="hr_cineboliviano_azul">
