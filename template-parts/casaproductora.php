<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 12-07-19
 * Time: 05:59 PM
 */

?>
<div class="col-md-4" style="text-align: center;">
    <?php
    if ( has_post_thumbnail() ) {
        the_post_thumbnail('full', ['class' => 'rounded mx-auto d-block', 'title' => 'Actor']);
    } else {
        echo "<img class='d-block w-100' src='".get_stylesheet_directory_uri().'/images/no-photo.png'."' />";
    }

    ?>
    <a href="<?=get_permalink()?>" class="btn btn-link"><?= the_title() ?></a>
</div>
