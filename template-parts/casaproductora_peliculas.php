<?php 
$casa_productora_id = get_query_var('casa_productora_id');
$args = array(  
        'post_type' => 'movie',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
        'order' => 'ASC',
);

$peliculas = get_posts( $args );

$peliculas_por_productora = [];
if ( $peliculas ) {
        foreach  ( $peliculas as $pelicula ) {
                $productoras = get_post_field('casa_productora', $pelicula->ID);
                if(in_array($casa_productora_id, $productoras)){
                        // echo $pelicula->ID." || ".get_post_field('duracion', $pelicula->ID)." || ".get_post_field('casa_productora', $pelicula->ID)." || ".$pelicula->post_title."<br>";
                        array_push($peliculas_por_productora, $pelicula);
                }
        }
        wp_reset_postdata();
}

$peliculas_ordenadas = array_chunk($peliculas_por_productora, 4);
?>
<h5>Películas en las que Trabajo:</h5>
<?php foreach ($peliculas_ordenadas as $key => $peliculas_4):?>
<div class="row">
        <?php foreach ($peliculas_4 as $key => $peli):?>
                <div class="col-md-3">
                        <a class="btn btn-link" style="padding-left: 0; text-decoration: none; color:#fbd518;" href="<?= get_permalink($peli->ID) ?>" data-toggle="popover" data-img="<?= get_the_post_thumbnail_url($peli->ID, 'thumbnail') ?>" title="<?= $peli->post_title ?>"><?= $peli->post_title ?></a>
                                
                </div>
        <?php endforeach ?>
</div>
<?php endforeach ?>
<br>
<script>
        jQuery(document).ready(function(){
         jQuery('[data-toggle="popover"]').popover({
          //trigger: 'focus',
                  trigger: 'hover',
          html: true,
          content: function () {
                                return '<img class="img-fluid" src="'+jQuery(this).data('img') + '" />';
          },
          title: 'Toolbox'
    }) 
        });
</script>