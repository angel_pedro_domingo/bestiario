<?php
get_header();
?>
    <div class="col-md-12">
        <?php if ( have_posts() ) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-md-1" style="text-align: center;">
                    <?php
                    if(get_the_post_thumbnail_url()){
                        echo "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='".get_the_post_thumbnail_url($the_ID, array('150' , '150'))."' /></a>";
                    } else {
                        echo "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='".get_stylesheet_directory_uri()."/images/user.png' /></a>";
                    }
                    ?>
                </div>
                <div class="col-md-11 cb_archive_movie" style="padding-top: 5px;">
                    <h4 class="entry-title"><?php the_title(); ?></h4>

                </div>
            </div>
        <?php
        endwhile; // end of the loop.
//        wp_pagenavi();
        ?>
        <div class="nav-previous alignleft"><?php previous_posts_link( 'Older posts' ); ?></div>
        <div class="nav-next alignright"><?php next_posts_link( 'Newer posts' ); ?></div>

        <?php else : ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>