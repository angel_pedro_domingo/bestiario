<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 27-06-19
 * Time: 12:26 PM
 */
?>
<form id="searchform" class="form-inline" method="get" action="<?php echo home_url('/'); ?>">
    <div class="form-group mx-sm-3 mb-2">
        <input type="text" class="form-control" name="s" placeholder="Buscar" value="<?php the_search_query(); ?>">
    </div>
    <button type="submit" class="btn btn-danger mb-2"><i class="fas fa-search"></i></button>
</form>
