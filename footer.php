<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container pt-3 pb-3">
            <div class="row">
                <div class="col-md-3">
                    <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="200">
                        </a>
                    <?php else : ?>
                        <a class="site-title-cine" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer',
                        'menu_id' => 'footer-menu',
                        'menu_class'      => 'nav-footer-bestiario',
                    ) );
                    ?>
                </div>
                <div class="col-md-3 bestiario-footer-texts">
                    <p>Universidad Privada Boliviana</p>
                    <p>Campus Julio Léon Prado</p>
                    <p>Av. Victor Ustariz Km 6.5, Cochabamba</p>
                    <p>Bolivia</p>

                </div>
                <div class="col-md-3 bestiario-footer-texts">
                    <p>Bestiario es un Proyecto de:</p>
                    <img src="<?php echo get_stylesheet_directory_uri().'/images/Artboard7-limpio.png'; ?>" class="img-responsive" width="140" style="padding-bottom: 4px;">
                    <img src="<?php echo get_stylesheet_directory_uri().'/images/lct-235.png'; ?>" class="img-responsive" width="140" style="padding-bottom: 4px;">
                    <img src="<?php echo get_stylesheet_directory_uri().'/images/citiee-235.png'; ?>" class="img-responsive" width="140">
                </div>
            </div>
            <div class="site-info" style="text-align: center;">
                <p style="margin-bottom: 0;"><a href="http://www.upb.edu">&copy;UPB</a> - <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?> <?php echo date('Y'); ?></p>
                <p class="credits">All rights reserved.</p>

            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function() {
        jQuery('#menu-menu-footer > li').addClass('nav-item');
        jQuery('#menu-menu-footer > li > a').addClass('nav-link');
        jQuery('#menu-menu-footer > li:first').remove();
    });
</script>
</body>
</html>