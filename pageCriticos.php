<?php
/**
 * Template Name: Pagina Criticos / autores
 */

get_header();?>

<?php while (have_posts()):the_post();
the_content();
get_template_part('template-parts/content-criticos', 'page');
// If comments are open or we have at least one comment, load up the comment template.
if (comments_open() || get_comments_number()):
comments_template();
endif;
endwhile;// End of the loop.
?>

<?php
get_sidebar();
get_footer();
