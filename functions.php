<?php
/**
 * Created by PhpStorm.
 * User: citiee
 * Date: 27-06-19
 * Time: 10:18 AM
 */
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
add_theme_support('post-thumbnails');
add_image_size( 'custom-cine-slider-main', 1024, 512, array('center', 'center'));
add_image_size( 'custom-cine-slider-main-512', 512, 350, array('center', 'center'));
add_image_size( 'custom-cine-slider-main-150', 150, 100, array('center', 'center'));

add_image_size( 'custom-cine-slider-main-512x260', 512, 260, array('center', 'center'));

function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );

}

/**
 * Add search box to primary menu
 */
function wpgood_nav_search($items, $args) {
    // If this isn't the primary menu, do nothing
    if( !($args->theme_location == 'primary') )
        return $items;
    // Otherwise, add search form
    return '<li>' . get_search_form(false) . '</li>'.$items;
}
add_filter('wp_nav_menu_items', 'wpgood_nav_search', 10, 2);

function getDirector(){
    echo 'ddddddd';
}

function htmlPersonas($personas){
    $str_personas = '';
    foreach ($personas as $persona) {
        $str_personas .= "<a href='" . get_post_field('guid', $persona->ID) . "' ";
        $str_personas .= "data-toggle='popover' ";
        $str_personas .= "data-img='".get_the_post_thumbnail_url($persona->ID)."' ";
        $str_personas .= "data-nombre='".get_the_title($persona->ID)."' ";
        $str_personas .= "data-info='".wp_trim_words(get_post_field('post_content', $persona->ID), 20, '...' )."' ";
        $str_personas .= "class='btn btn-link btn-link-cine link-cine-per-popover' style='color: #000000 !important;'>";
        $str_personas .= get_post_field('post_title', $persona->ID) . '</a> - ';
    }
    return substr($str_personas, 0, -3);
}
function htmlCasaProductora($casas){
    $str_casas = '';
    foreach ($casas as $casa) {
        $str_casas .= '<div class="col-md-3 cb_casa_productora">';
        $str_casas .= "<h4>".$casa->post_title."</h4>";
        $str_casas .= "<a href='" . get_post_field('guid', $casa->ID) . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='".get_the_post_thumbnail_url($casa->ID, array('150' , '150'))."' /></a>";
        $str_casas .= '</div>';
    }
    return $str_casas;
}

//function htmlResenia($resenias){
//    $str_resenias = '';
//    foreach ($resenias as $resenia) {
//        $str_resenias .= '<div class="col-md-3 cb_casa_productora">';
//        $str_resenias .= "<h4>".$resenia->post_title."</h4>";
//        if(!empty(get_the_post_thumbnail_url($resenia->ID, 'full')))
//            $str_resenias .= "<a href='" . get_post_field('guid', $resenia->ID) . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='".get_the_post_thumbnail_url($resenia->ID, 'full')."' /></a>";
//        else {
//            $str_resenias .= "<a href='" . get_post_field('guid', $resenia->ID) . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='".get_stylesheet_directory_uri().'/images/no-photo.png'."' /></a>";
//        }
//        $str_resenias .= '</div>';
//    }
//    return $str_resenias;
//}

function htmlResenia($resenias){
    $str_resenias = '';
    foreach ($resenias as $resenia) {
        $str_resenias .= '<div class="col-md-12 cb_casa_productora" style="text-align: left; text-align: justify; text-justify: inter-word;">';
        $content_sin_html = wp_strip_all_tags(strip_shortcodes($resenia->post_content));
        if(strlen($content_sin_html) > 150) {
            $content_sin_html = substr($content_sin_html, 0, 150).'...';
        }

        $str_resenias .= '<p>'.$content_sin_html.' '."<a href='" . get_post_field('guid', $resenia->ID) . "' class='btn btn-link btn-link-cine'>Leer más</a>".'</p>';
        $str_resenias .= '</div>';
    }
    return $str_resenias;
}

function htmlPeliculasByPersona($peliculas, $rol){
    $str_peliculas = "";
    foreach ($peliculas as $pelicula){
        $directores = get_post_field($rol, $pelicula->ID);
        foreach ($directores as $director){
            if(strcmp(get_post_field('post_title', $director), get_the_title()) == 0){
                $str_peliculas .= "<a href='" . get_post_field('guid', $pelicula->ID) . "' class='btn btn-link btn-link-cine'>".get_the_title($pelicula->ID)."</a> - ";
                $str_peliculas .= "";
                $str_peliculas .= "";
                $str_peliculas .= "";
            }
        }
    }
    return substr($str_peliculas, 0, -3);
}

function htmlCriticosListLink($criticos){
    $str_criticos = "";
    foreach ($criticos as $per){
        foreach ($per as $p) {
            $person = get_post($p);
            $str_criticos .= "<a href='" . get_post_field('guid', $person->ID) . "' class='btn btn-link btn-link-cine'>".get_the_title($person->ID)."</a> - ";
        }

    }

//    return $str_criticos;
    return substr($str_criticos, 0, -3);
}

function htmlCriticasByPersona($criticas){
    $str_criticas = "";
    foreach ($criticas as $critica){
        $str_criticas .= "<a href='" . get_post_field('guid', $critica->id) . "' class='btn btn-link btn-link-cine'>".$critica->post_title."</a> - ";
    }

    return substr($str_criticas, 0, -3);
}

function custom_posts_per_page( $query ) {
    if ( $query->is_archive('project') ) {
        set_query_var('posts_per_page', 5);
    }
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );

function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

function fancyBoxImagen($index, $imagen1, $imagen2)
{
    $html = "<div>";
    $html .= "<a id='single_".$index."' href='".$imagen2."' >";
    $html .= "<img src='".$imagen1."' alt=''/>";
    $html .= "</a>";
    $html .= "</div>";
    return $html;
}

// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
    'primary' => esc_html__( 'Primary', 'wp-bootstrap-starter' ),
    'footer' => esc_html__( 'Footer Menu', 'wp-bootstrap-starter' ),
) );


function get_youtube_video_ID($youtube_video_url) {
    $video_id = explode("?v=", $youtube_video_url);

// echo "zzzzz1: ".$video_id[0];
    if(count($video_id) > 1)
        return $video_id[1];
    else{
        echo "zzzzz2";
        $video_id = explode("&v=", $youtube_video_url);
        if(count($video_id) >0){
            return $video_id[1];
        }
    }
    return false;
}

function get_youtube_id_from_url_bestiario($url)  {
     preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $results);    return $results[6];
}

require_once trailingslashit( get_stylesheet_directory() ) . '/inc/template-tags.php';