<?php
get_header();
?>
    <div class="col-md-12">
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= "<a href='" . get_permalink() . "' class='btn btn-link btn-link-cine'><img class='img-thumbnail' src='" . get_field('afiche') . "' /></a>" ?>
                    </div>
                    <div class="col-md-9 cb_archive_movie" style="padding-top: 5px;">
                        <h4 class="entry-title"><?php the_title(); ?></h4>
                        <hr class="hr_cineboliviano">
                        <p><b>Año:</b> <?= get_field('anio') ?></p>
                        <p><b>Duracion:</b> <?= get_field('duracion') ?> min.</p>
                        <p><b>Sinopsis:</b></p>
                        <p><?= wp_trim_words(get_field('sinopsis'), 150) ?></p>
                    </div>
                </div>
                <!--    <div class="row">-->
                <!--        <div class="col-md-3">-->
                <!--            sss-->
                <!--        </div>-->
                <!--        <div class="col-md-9"></div>-->
                <!--    </div>-->
            <?php endwhile; // end of the loop. ?>
            <div class="nav-previous alignleft"><?php previous_posts_link('Older posts'); ?></div>
            <div class="nav-next alignright"><?php next_posts_link('Newer posts'); ?></div>

        <?php else : ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>