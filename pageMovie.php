<?php
/**
 * Template Name: Page Peliculas Cortas
 */

get_header();?>
<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<?php while (have_posts()):the_post();
the_content();
get_template_part('template-parts/content-movie-cortas', 'page');
// If comments are open or we have at least one comment, load up the comment template.
if (comments_open() || get_comments_number()):
comments_template();
endif;

endwhile;// End of the loop.
?>
</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
